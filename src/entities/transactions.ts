import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Transactions {
	@PrimaryGeneratedColumn() id: number;

	@Column() block_number: number;

	@Column() block_hash: string;

	@Column() date_time: Date;

	@Column() tx_hash: string;

	@Column() gas: number;

	@Column() gas_price: string;
}
