import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Settings {
	@PrimaryGeneratedColumn() id: number;

	@Column({ default: 0 })
	blockHeightProcessed: number;
}
