require('dotenv').config();
const Web3 = require('web3');

export const dbConfig = {
	host: process.env.DB_HOST,
	port: process.env.DB_PORT,
	username: process.env.DB_USER,
	password: process.env.DB_PWD,
	database: process.env.DB_NAME
};

export const infura = {
	projectId: process.env.PROJECT_ID,
	projectSecret: process.env.PROJECT_SECRET,
	httpProvider: new Web3(new Web3.providers.HttpProvider(`https://ropsten.infura.io/v3/${process.env.PROJECT_ID}`))
};

export const contract = {
	creator: process.env.CONTRACT_CREATOR,
	address: process.env.CONTRACT_ADDRESS,
	secretKey: process.env.SECRET_KEY
};
