import * as Utils from '../utils';
import * as Services from '../services';

const reRun = async () => {
	console.log(`\n`);
	await Utils.timeout(2000);
	return syncBlock();
};

export const syncBlock = async () => {
	// Variables
	let blockInfo: Object = null;
	let latestBlock: number,
		processedBlock: number = null;

	// Get Latest Block
	try {
		latestBlock = await Services.getLastestBlock();
	} catch (e) {
		return reRun();
	}

	// Check Processed Block
	try {
		processedBlock = await Services.getProcessedBlock();
	} catch (e) {
		return reRun();
	}

	console.log(`Latest Block: ${latestBlock}`);
	console.log(`Processed Blocks: ${processedBlock}`);

	// Checking if it is in the latest block already
	if (latestBlock === processedBlock) {
		console.log('In the latest block...please wait for the new block');
		return reRun();
	}

	let blockToProcess = processedBlock + 1;
	console.log(`Next Process Block: ${blockToProcess}`);

	// Get block information
	try {
		blockInfo = await Services.getBlockInfo(blockToProcess);
	} catch (e) {
		return reRun();
	}

	// Get Transactions
	let transactions = blockInfo['transactions'];
	if (!transactions || (transactions && transactions.length === 0)) {
		await Services.updateProcessedBlock(blockToProcess);
		console.log(`Block ${blockToProcess} does not have transcation`);
		return reRun();
	}

	// Get Transaction Detail
	for (const hash of transactions) {
		try {
			let transaction = await Services.getTransaction(hash);

			// Check if the transaction is already in database
			let dbTransaction = await Services.getDbTransaction(hash);
			if (dbTransaction) continue;

			// Save transaction to Database
			let transactionObj = {
				blockNumber: blockInfo['number'],
				blockHash: blockInfo['hash'],
				dateTime: new Date(blockInfo['timestamp'] * 1000),
				txHash: transaction['hash'],
				gas: transaction['gas'],
				gasPrice: transaction['gasPrice']
			};
			await Services.saveDbTransaction(transactionObj);
		} catch (e) {
			continue;
		}
	}

	// Update the block in settings
	try {
		await Services.updateProcessedBlock(blockToProcess);
	} catch (e) {
		return reRun();
	}

	// Wait a while before syncing other block
	return reRun();
};
