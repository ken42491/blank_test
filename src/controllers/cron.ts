import * as Services from '../services';
import * as Configs from '../config';
const erc20_json = require('../../abi/erc20_abi.json');

export const snapshot = async () => {
	console.log(`Snapshot is progress...`);
	// Getting yesterday's data
	const today = new Date();
	const yesterday = new Date(today);
	yesterday.setDate(yesterday.getDate() - 1);
	let day = yesterday.getDate();
	let month = yesterday.getMonth() + 1;
	let formattedDay = `${yesterday.getFullYear()}-${month < 10 ? '0' + month : month}-${day < 10 ? '0' + day : day}`;

	// Get total number of blocks & total ETH spent on gas fees yesterday
	let transactions: any = [];
	try {
		transactions = await Services.getDbTransactions(
			`"date_time" between '${formattedDay} 00:00:00' and '${formattedDay} 23:59:59'`
		);
	} catch (e) {
		console.log('Failed to get transactions');
		return;
	}

	let totalEthSpent = 0;
	let totalNumberOfBlocks = 0;
	let largestBlock = null;
	let smallestBlock = null;
	for (const transaction of transactions) {
		let ethSpent = transaction['gas'] * parseFloat(transaction['gas_price']);
		let tempBlock = transaction['block_number'];
		totalEthSpent = totalEthSpent + ethSpent;
		if (!largestBlock || !smallestBlock) {
			largestBlock = transaction['block_number'];
			smallestBlock = transaction['block_number'];
		}

		if (largestBlock < tempBlock) largestBlock = tempBlock;
		if (smallestBlock > tempBlock) smallestBlock = tempBlock;
	}
	totalNumberOfBlocks = largestBlock - smallestBlock + 1;

	// Contract variables
	const abi = erc20_json;
	let gas, gasPrice, data;
	const web3 = Configs.infura.httpProvider;
	try {
		const contract = await Services.getContract(abi, Configs.contract.address, Configs.contract.creator);
		data = contract.methods.storeAllData(totalNumberOfBlocks, web3.utils.toBN(totalEthSpent)).encodeABI();
		gas = await Services.getEstimateGas(1, Configs.contract.address, 0, data);
		gas = web3.utils.toBN(gas);
		gasPrice = await Services.getGasPrice();
	} catch (e) {
		console.log(`Failed to get contract variables ${e}`);
	}

	// Set up transaction payload
	let transactionPayload = {
		from: Configs.contract.creator,
		to: Configs.contract.address,
		value: 0,
		gas,
		gasPrice,
		data
	};

	// Add account
	try {
		await Services.setDefaultAccount();
	} catch (e) {
		console.log('Setting default account error', e);
		return;
	}

	// Send Transaction
	try {
		await Services.sendTransaction(transactionPayload);
		console.log(`Sent to blockchain!`);
	} catch (e) {
		console.log('Sending transaction error', e);
		return;
	}
};
