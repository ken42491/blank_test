export type Transaction = {
	blockNumber: number;
	blockHash: string;
	dateTime: Date;
	txHash: string;
	gas: number;
	gasPrice: string;
};
