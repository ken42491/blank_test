import express from 'express';
import cors from 'cors';
import * as Controllers from './controllers';

const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

// API Routes
app.get('/', (_, res) => {
	res.send('Hello World! Server is up.');
});

const server = app.listen(7000, async () => {
	await Controllers.syncBlock();
	return;
});

// Cron job
var cron = require('node-cron');
cron.schedule('5 0 * * *', Controllers.snapshot);

server.timeout = 1800000;
