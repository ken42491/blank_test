import { createConnection, getConnection, ConnectionOptions } from 'typeorm';
import * as Configs from '../config';

// DB Connection
const connectionConfig: ConnectionOptions = {
	name: 'connection',
	type: 'postgres',
	host: Configs.dbConfig.host,
	port: 5432,
	username: Configs.dbConfig.username,
	password: Configs.dbConfig.password,
	database: Configs.dbConfig.database,
	entities: [ __dirname + '/../entities/*{.ts,.js}' ],
	synchronize: true
};

export const connection = createConnection(connectionConfig);

export const establishConnection = () => {
	return getConnection('connection');
};
