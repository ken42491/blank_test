import { establishConnection } from '../connections/connection';
import { Settings } from '../entities/settings';
import * as Configs from '../config';

export const getBlockInfo = async (number) => {
	return new Promise<Object>(async (resolve, reject) => {
		try {
			const web3 = Configs.infura.httpProvider;
			const data = web3.eth.getBlock(number);
			if (!data) return reject(false);
			return resolve(data);
		} catch (e) {
			return reject(e);
		}
	});
};

export const getLastestBlock = async () => {
	return new Promise<number>(async (resolve, reject) => {
		try {
			const web3 = Configs.infura.httpProvider;
			const data = web3.eth.getBlockNumber();
			if (!data) return reject(false);
			return resolve(data);
		} catch (e) {
			return reject(e);
		}
	});
};

export const getProcessedBlock = async () => {
	return new Promise<number>(async (resolve, reject) => {
		try {
			const connection = establishConnection();
			const repo = connection.getRepository(Settings);
			const data = await repo.findOne();
			if (!data) return reject(false);
			return resolve(data['blockHeightProcessed']);
		} catch (e) {
			return reject(e);
		}
	});
};

export const updateProcessedBlock = async (blockHeightProcessed) => {
	return new Promise<boolean>(async (resolve, reject) => {
		try {
			const connection = establishConnection();
			const repo = connection.getRepository(Settings);
			const data = await repo.findOne();
			let updateData = {
				...data,
				blockHeightProcessed
			};
			repo.save(updateData);
			return resolve(true);
		} catch (e) {
			return reject(e);
		}
	});
};
