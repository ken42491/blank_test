import * as Configs from '../config';

export const getEstimateGas = (speed = 1, toAddress, value, contractData = null) => {
	return new Promise(async (resolve, reject) => {
		try {
			const web3 = Configs.infura.httpProvider;
			const gasAmount = await web3.eth.estimateGas({
				to: toAddress,
				data: contractData,
				value: value
			});
			if (gasAmount) return resolve(parseFloat(gasAmount) * speed);
			return resolve(null);
		} catch (e) {
			return reject(e);
		}
	});
};

export const getGasPrice = () => {
	return new Promise(async (resolve, reject) => {
		try {
			const web3 = Configs.infura.httpProvider;
			const gasPrice = await web3.eth.getGasPrice();
			if (!gasPrice) return reject(false);
			return resolve(gasPrice);
		} catch (e) {
			return reject(e);
		}
	});
};
