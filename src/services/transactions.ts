import { establishConnection } from '../connections/connection';
import * as Configs from '../config';
import { Transactions } from '../entities/transactions';
import { Transaction } from '../types/transaction';

export const getTransaction = async (hash) => {
	return new Promise<Object>(async (resolve, reject) => {
		try {
			const web3 = Configs.infura.httpProvider;
			const data = web3.eth.getTransaction(hash);
			if (!data) return reject(false);
			return resolve(data);
		} catch (e) {
			return reject(e);
		}
	});
};

export const getDbTransaction = async (hash) => {
	return new Promise<Object | null>(async (resolve, reject) => {
		try {
			const connection = establishConnection();
			const repo = connection.getRepository(Transactions);
			const data = await repo.findOne({ where: `"tx_hash" = '${hash}'` });
			return resolve(data);
		} catch (e) {
			return reject(e);
		}
	});
};

export const getDbTransactions = async (arg) => {
	return new Promise<Object | null>(async (resolve, reject) => {
		try {
			const connection = establishConnection();
			const repo = connection.getRepository(Transactions);
			const data = await repo.find({ where: `${arg}` });
			return resolve(data);
		} catch (e) {
			return reject(e);
		}
	});
};

export const saveDbTransaction = async (object: Transaction) => {
	return new Promise<Object | null>(async (resolve, reject) => {
		try {
			const connection = establishConnection();
			const repo = connection.getRepository(Transactions);
			let data = new Transactions();
			data = {
				id: null,
				block_number: object['blockNumber'],
				block_hash: object['blockHash'],
				date_time: object['dateTime'],
				tx_hash: object['txHash'],
				gas: object['gas'],
				gas_price: object['gasPrice']
			};
			let savedData = repo.save(data);
			return resolve(savedData);
		} catch (e) {
			return reject(e);
		}
	});
};

export const sendTransaction = (transactionPayload) => {
	return new Promise(async (resolve, reject) => {
		const web3 = Configs.infura.httpProvider;
		web3.eth.sendTransaction(transactionPayload, (err, hash) => {
			if (err) return reject(err);
			if (hash) return resolve(hash);
			return resolve(null);
		});
	});
};

export const signTransaction = (transactionPayload) => {
	return new Promise(async (resolve, reject) => {
		const web3 = Configs.infura.httpProvider;
		web3.eth.signTransaction(transactionPayload, Configs.contract.secretKey, (err, hash) => {
			if (err) return reject(err);
			if (hash) return resolve(hash);
			return resolve(null);
		});
	});
};
