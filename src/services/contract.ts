import * as Configs from '../config';

export const getContract = async (abi, contractAddress, from) => {
	return new Promise<any>(async (resolve, reject) => {
		try {
			const web3 = Configs.infura.httpProvider;
			const contract = new web3.eth.Contract(abi, contractAddress, {
				from: from
			});
			if (!contract) return reject(false);
			return resolve(contract);
		} catch (e) {
			return reject(e);
		}
	});
};
