import * as Configs from '../config';

export const setDefaultAccount = async () => {
	try {
		const web3 = Configs.infura.httpProvider;
		await web3.eth.accounts.wallet.add(Configs.contract.secretKey);
		return;
	} catch (e) {
		return e;
	}
};

export const unlockAccount = async (address) => {
	return new Promise<any>(async (resolve, reject) => {
		try {
			const web3 = Configs.infura.httpProvider;
			const status = await web3.eth.personal.unlockAccount(address, Configs.contract.secretKey, 600);
			if (!status) return reject(false);
			return resolve(status);
		} catch (e) {
			return reject(e);
		}
	});
};
