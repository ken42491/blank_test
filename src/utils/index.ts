import { checkRequiredFields, returnApiResponse } from './api';
import { getIp } from './ip';

const timeout = (ms) => {
	return new Promise((resolve) => setTimeout(resolve, ms));
};

export { checkRequiredFields, returnApiResponse, getIp, timeout };
