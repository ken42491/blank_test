### Global Dependencies

- Node version please use 16.13.0

Set up the project
```
npm install -g typeorm nodemon
npm install
touch .env
```

### Start the Project

To run the project
```
npm run start:live
```
or
```
npm run start
```

### To Test the project

```
npm run test
```

Working on the readme...