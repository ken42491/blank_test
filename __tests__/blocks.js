const { getBlockInfo, getLastestBlock, getProcessedBlock } = require('../dist/services/blocks');

describe('Blocks', () => {
	test('getBlockInfo will not return null', async () => {
		let data = await getBlockInfo(1);
		expect(data).not.toBe(null);
	});

	test('getLastestBlock will not return null', async () => {
		let data = await getLastestBlock();
		expect(data).not.toBe(null);
	});

	test('getProcessedBlock will not return null', async () => {
		let data = await getProcessedBlock(1);
		expect(data).not.toBe(null);
	});
});
