const { padZerosBehind, removeZerosBehind } = require('../dist/utils/format');

describe('Formatting Numbers', () => {
	test('Padding zero behind a number', () => {
		expect(padZerosBehind('10', 2)).toBe('1000');
	});

	test('Removing zero behind a number', () => {
		expect(removeZerosBehind('1000', 2)).toBe('10');
	});
});
